import {STATUS_INIT} from './constants';

// const isNode = typeof module !== 'undefined';

export default () => {
    return {
        filters: {
            query: null,
            params: {},
        },
        companies: [],
        pagination: null,
        status: STATUS_INIT
    }
}
