import {STATUS_INIT, STATUS_LOADING} from './constants';

export default {
    getCompanies(state) {
        return state.companies;
    },
    getPagination(state) {
        return state.pagination;
    },
    getFilters(state) {
        return state.filters;
    },
    getFiltersAsUrlParam(state) {
        let params = {};

        if (state.filters.query !== '') {
            params.query = state.filters.query;
        }

        for (let param in state.filters.params) {
            if (state.filters.params[param] !== false) {
                params['filters[' + param + ']'] = state.filters.params[param];
            }
        }

        return params;
    },
    isWait(state) {
        return [STATUS_INIT, STATUS_LOADING].indexOf(state.status) !== -1;
    },
};
