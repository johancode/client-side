export default {
    setCompanies(state, companies) {
        state.companies = companies;
    },
    addCompanies(state, companies) {
        companies.forEach(company => state.companies.push(company));
    },
    setPagination(state, pagination) {
        state.pagination = pagination;
    },
    addCompany(state, company) {
        state.companies.push(company);
    },
    setStatus(state, value) {
        state.status = value;
    },
    setFilters(state, value) {
        state.filters.query = value.query;
        state.filters.params = value.params;
    }
}
