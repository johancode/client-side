import {STATUS_LOADING, STATUS_READY} from './constants';

// const isNode = typeof module !== 'undefined';

export default {
    updateFilters({state, commit, dispatch}, newFilters) {
        commit('setFilters', newFilters);
        dispatch('fetch');
    },
    fetch({state, commit, getters}) {
        commit('setStatus', STATUS_LOADING);
        commit('setCompanies', []);
        commit('setPagination', {});

        return this
            .$axios
            .get('/api/companies', {
                params: getters.getFiltersAsUrlParam
            })
            .then(response => {
                commit('setCompanies', response.data.data);
                commit('setStatus', STATUS_READY);
                commit('setPagination', {
                    current: response.data.meta.current_page,
                    total: response.data.meta.total,
                    perPage: response.data.meta.per_page,
                });
            });
    },
    loadMore({state, commit, getters}) {
        commit('setStatus', STATUS_LOADING);

        return this
            .$axios
            .get('/api/companies', {
                params: {
                    page: state.pagination ? (state.pagination.current + 1) : 1,
                    ...getters.getFiltersAsUrlParam
                }
            })
            .then(response => {
                commit('addCompanies', response.data.data);
                commit('setStatus', STATUS_READY);
                commit('setPagination', {
                    current: response.data.meta.current_page,
                    total: response.data.meta.total,
                    perPage: response.data.meta.per_page,
                });
            });
    },
}
