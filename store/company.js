import state from "~/store/company/default-state";
import getters from "~/store/company/getters";
import mutations from "~/store/company/mutations";
import actions from "~/store/company/actions";

export {
    state,
    getters,
    mutations,
    actions,
};
