import state from "~/store/companies/default-state";
import getters from "~/store/companies/getters";
import mutations from "~/store/companies/mutations";
import actions from "~/store/companies/actions";

export {
    state,
    getters,
    mutations,
    actions,
};
