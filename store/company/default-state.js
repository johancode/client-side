export default () => {
    return {
        name: null,
        occupation: null,
        description: null,
        phone: null,
    }
}
