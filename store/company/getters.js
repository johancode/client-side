export default {
    getName(state) {
        return state.name;
    },
    getOccupation(state) {
        return state.occupation;
    },
    getDescription(state) {
        return state.description;
    },
    getPhone(state) {
        return state.phone;
    },
};
