export default {
    fetch({state, commit, getters}, companySlug) {
        return this
            .$axios
            .get('/api/companies/' + companySlug)
            .then(response => {
                commit('setName', response.data.name);
                commit('setOccupation', response.data.occupation);
                commit('setDescription', response.data.description);
                commit('setPhone', response.phone);
            });
    },
}
