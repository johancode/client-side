export default {
    setName(state, name) {
        state.name = name;
    },
    setOccupation(state, occupation) {
        state.occupation = occupation;
    },
    setDescription(state, description) {
        state.description = description;
    },
    setPhone(state, phone) {
        state.phone = phone;
    },
}
