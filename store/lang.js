export const state = () => ({
    locales: ['en', 'ru'],
    locale: 'en'
});


export const mutations = {
    setLocale(state, locale) {
        if (state.locales.includes(locale)) {
            state.locale = locale;

            this.$cookies.set('locale', locale);
        }
    }
};
