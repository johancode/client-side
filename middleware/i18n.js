export default function ({isHMR, app, store, route, params, error, redirect}) {
    const defaultLocale = app.i18n.fallbackLocale;
    const cookieLocale = app.$cookies.get('locale');


    // If middleware is called from hot module replacement, ignore it
    if (isHMR) {
        return;
    }

    // Get locale from params
    const locale = route.query.hasOwnProperty('lang') ? route.query.lang : (cookieLocale ? cookieLocale : defaultLocale);
    if (!store.state.lang.locales.includes(locale)) {
        return error({message: 'This page could not be found.', statusCode: 404});
    }


    // Set locale
    store.commit('lang/setLocale', locale);
    app.i18n.locale = store.state.lang.locale;

    // If route has defaultLocale -> redirect to /
    // if (route.query.hasOwnProperty('lang') && route.query.lang === defaultLocale) {
    // const toReplace = '^/' + defaultLocale + (route.fullPath.indexOf('/' + defaultLocale + '/') === 0 ? '/' : '');
    // const re = new RegExp(toReplace);
    // return redirect(route.fullPath.replace(re, '/'));
    // }
}
