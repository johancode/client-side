export default {
    server: {
        host: '0.0.0.0',
        port: 3002,
    },
    mode: 'universal',
    head: {
        title: 'Houserepair.info',
        meta: [
            {charset: 'utf-8'},
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || '',
            },
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
            {
                href: 'https://fonts.googleapis.com/css?family=Exo+2:400,400i,700,700i|Oswald:700',
                rel: 'stylesheet',
            },
        ],
    },
    loading: {color: '#fff'},
    router: {
        middleware: 'i18n'
    },
    css: ['@/assets/sass/main.sass'],
    plugins: [
        '~/plugins/i18n.js',
        '~/plugins/axios'
    ],
    buildModules: [],
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/dotenv',
        '@nuxtjs/auth',
        '@nuxtjs/proxy',
        'cookie-universal-nuxt',
    ],
    axios: {
        proxy: true,
        progress: false,
    },
    auth: {
        strategies: {
            local: {
                endpoints: {
                    login: {
                        url: '/api/auth/login',
                        method: 'POST',
                        propertyName: 'token',
                    },
                    user: {
                        url: '/api/auth/status',
                        method: 'GET',
                        propertyName: 'user',
                    },
                    logout: {
                        url: '/api/auth/logout',
                        method: 'POST',
                    },
                },
            },
        },
        redirect: {
            login: '/auth/login',
        },
    },
    proxy: {
        '/api': {
            // target: 'http://api.houserepair.local',
            target: 'http://hr_nginx:8080',
            pathRewrite: {
                '^/api': '',
            },
        },
    },
    build: {
        postcss: {
            preset: {
                features: {
                    customProperties: false,
                },
            },
        },
        extend(config, ctx) {
        },
    },
}
