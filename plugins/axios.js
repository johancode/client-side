export default function ({$axios, store, redirect}) {
    $axios.onError(error => {
        if (error.response && error.response.status === 401) {
            $axios.setToken(false);
            window.location.reload();
        }
    });
}
