FROM node:13-alpine

#RUN addgroup -g 1000 www \
#    && adduser -u 1000 -G www -s /bin/sh -D www
#
#USER www

WORKDIR /app

EXPOSE 3002
CMD [ "yarn", "run", "start" ]
